const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};

const app = express();

app.set('view engine', 'ejs');
app.use(expressLayouts);

var router = require('./app/routes');

app.use('/', router);
app.use(express.static(__dirname + '/public'));
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(8181, () => {
  console.log('listening man');
});
