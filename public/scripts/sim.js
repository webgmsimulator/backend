var SIM = window.SIM = window.SIM || {};

 SIM.state = {
    'puckOwner': null,
    'zones': ['D', 'N', 'O'],
    'periods': [20, 20, 20],
    'teams': {
        'HT': {
            'infos': null,
            'onIce': [],
            'goalie': null,
            'shots': null,
            'penalties': null
        },
        'AT': {
            'infos': null,
            'onIce': [],
            'goalie': null,
            'shots': null,
            'penalties': null
        }
    }
 };

 SIM.events = {};

 SIM.initialize = function() {
    this.initializeTeams();
    this.createEvents();
    this.updateDisplays();
    this.broadcastEvents();
 }

  SIM.initializeTeams = function() {
    var sim = this;
    _.each(this.state.teams, function(team, key) {
        var starting;

        if(key === 'HT') {
            if(homeTeam) {
                team.infos = homeTeam;
            }
        } else {
            if(awayTeam) {
                team.infos = awayTeam;
            }
        }

        starting = Object.assign(team.infos.lines[0].playerPos, team.infos.defense[0].playerPos);
        team.onIce = sim.changePlayers(starting, team);
        team.goalie = sim.changePlayers({"G" : team.infos.goalie}, team);
    });
 }

 SIM.createEvents = function() {
    this.events.roll = new Event('roll');
 }

 SIM.updateDisplays = function() {
    var display;
    // HTDis.querySelector('.onIce').innerHTML = this.state.teams.HT.onIce;
    // ATDis.querySelector('.onIce').innerHTML = this.state.teams.AT.onIce;

    _.each(this.state.teams, function(team, key) {
        if(key === 'HT') {
            display = document.querySelector('.homeTeamDisplay');
        } else {
            display = document.querySelector('.awayTeamDisplay');
        }

        display.querySelector('.name').innerHTML = team.infos.name;
        display.querySelector('.onIce').innerHTML = team.onIce.map(function(player) {
            return '#' + player.number + ' ' + player.name;
        });
        display.querySelector('.goalie').innerHTML = team.goalie.map(function(goalie) {
            return '#' + goalie.number + ' ' + goalie.name;
        });
    });

 }

 SIM.broadcastEvents = function() {
    console.log('broadcastEvents SIM');
    // window.dispatchEvent(this.events.roll);
 }

 SIM.changePlayers = function(players, team) {
    var result;
    return _.map(players, function(x) {
        var player = team.infos.proPlayers.find( function(y) {
            return y.id === x;
        });
        return new Player(player);
    });
 }

    window.addEventListener('load', function() {
        SIM.initialize();
    });
