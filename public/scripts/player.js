export default class Player(playerData) {
    constructor(playerData) {
      // this.id = id;
      this.simulator = window.SIM;

      this.name = playerData.name;
      this.number = playerData.number;
      this.position = playerData.position;
      this.shoots = playerData.shoots;
      this.condition = playerData.condition;
      this.injured = playerData.injured;
      this.intensity = playerData.intensity;
      this.speed = playerData.speed;
      this.strength = playerData.strength;
      this.endurance = playerData.endurance;
      this.durability = playerData.durability;
      this.discipline = playerData.discipline
      this.skating = playerData.skating;
      this.passing = playerData.passing;
      this.puckControl = playerData.puckControl;
      this.defense = playerData.defense;
      this.scoring = playerData.scoring;
      this.experience = playerData.experience;
      this.leadership = playerData.leadership;

      this.energy;
      this.init();
    }

    init() {
        console.log("Sup dawg, I'm " + this.name);
        this.bindEvents();
    }
    bindEvents() {
        this.addEventListener('roll', this.broadcastEvents);
    }
    broadcastEvents() {
        console.log('broadcastEvents');
    }
    // rollPass: function() {
    //
    // },
    // rollPuckCarry: function() {
    //
    // },
    // rollShoot: function() {
    //
    // },
    // rollClear: function() {
    //
    // },
    // rollIntercept: function() {
    //
    // },
    // rollSteal: function() {
    //
    // },
    // rollBlock: function() {
    //
    // },
    // rollSave: function() {
    //
    // },
    // rollBodyCheck: function() {
    //
    // }
};
