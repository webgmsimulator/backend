'use strict';
const express = require('express');
const path = require('path');

const router = express.Router();

module.exports = router;

router.get('/', function(request, responce){
    responce.render('pages/index');
});

router.get('/about', function(request, responce){
    responce.render('pages/about');
});

router.get('/add-player', function(request, responce) {
    responce.render('pages/addPlayer');
});
router.post('/add-player', function(request, responce) {

});

router.get('/sim', function(request, responce) {
    responce.render('pages/sim');
});
